/* list.h: List Structure */
#include "lsort.h"
#include <stdlib.h>
#include <string.h>
/* Internal Functions */
Node *  node_reverse(Node *curr, Node *prev);
Node *  list_merge_sort(Node *head);
void    list_split(Node *head, Node **left, Node **right);
Node *  list_merge(Node *left, Node *right);
/* External Functions */
/**
 * * Create list.
 * *
 * * This allocates a list that the user must later deallocate.
 * * @return  Allocated list structure.
 * */
List *  list_create() {
   List *created = malloc(sizeof(List));
   created->head = NULL;
   created->tail = NULL;
   created->size = 0;
   return created;
}
/**
 * * Delete list.
 * *
 * * This deallocates the given list along with the nodes inside the list.
 * * @param   l       List to deallocate.
 * * @return  NULL pointer.
 * */
List *  list_delete(List *l) {
    node_delete(l->head, true);
    free(l);
    return NULL;
}
/**
 * * Push back.
 * *
 * * This adds a new node containing the given string to the back of the list.
 * * @param   l       List structure.
 * * @param   s       String.
 * */
void    list_push_back(List *l, char *s) {
    Node *newNode = node_create(s, NULL);
    if (l->tail) {
        (l->tail)->next = newNode;
        l->tail = newNode;
    }
    else {
        l->head = newNode;
        l->tail = newNode;
    }
    l->size++;
}
/**
 * * Reverse list.
 * *
 * * This reverses the list.
 * * @param   l       List structure.
 * */
void    list_reverse(List *l) {
    l->tail = l->head;
    l->head = node_reverse(l->head, NULL);
}
/**
 * * Reverse node.
 * *
 * * This internal function recursively reverses the node.
 * * @param   curr    The current node.
 * * @param   prev    The previous node.
 * * @return  The new head of the singly-linked list.
 * */
Node *  node_reverse(Node *curr, Node *prev) {
    Node *tail = curr;
    if (curr->next) {
        tail = node_reverse(curr->next, curr);
    }
    curr->next = prev;
    return tail;
}
/**
 * * Sort list using merge sort.
 * *
 * * This sorts the list using a custom implementation of merge sort.
 * * @param   l       List structure.
 * */
void  list_sort(List *l) {
    Node *pointer;
    l->head = list_merge_sort(l->head);
    pointer = l->head;
    while (pointer->next) {
        pointer = pointer->next;
    }
    l->tail = pointer;
}
/**
 * * Performs recursive merge sort.
 * *
 * * This internal function performs a recursive merge sort on the singly-linked
 * * list starting with head.
 * * @param   head    The first node in a singly-linked list.
 * * @return  The new head of the list.
 * */
Node *  list_merge_sort(Node *head) {
    Node *left = head;
    Node *right = head;
    if (head->next) {
        list_split(head, &left, &right);
        left = list_merge_sort(left);
        right = list_merge_sort(right);
        head = list_merge(left, right);
    }
    return head;
}
/**
 * * Splits the list.
 * *
 * * This internal function splits the singly-linked list starting with head into
 * * left and right sublists.
 * * @param   head    The first node in a singly-linked list.
 * * @param   left    The left sublist.
 * * @param   right   The right sublist.
 * */
void    list_split(Node *head, Node **left, Node **right) {
    if (!head->next) {
        return;
    }
    Node *fast = head;
    Node *slow = head;
    while(fast != NULL && fast->next != NULL) {
        slow = slow->next;
        fast = fast->next->next;
    }
    *left = head;
    while (head->next != slow) {
        head = head->next;
    }
    head->next = NULL;
    *right = slow;
}
/**
 * * Merge sublists.
 * *
 * * This internal function merges the left and right sublists into one ordered
 * * list.
 * * @param   left    The left sublist.
 * * @param   right   The right sublist.
 * * @return  The new head of the list.
 * */
Node *  list_merge(Node *left, Node *right) {
    Node* head = node_create("tempHead", NULL);
    Node* tail = head;
    while (right && left) {
        if ( strcmp(left->data, right->data) > 0 ) {
            tail->next = right;
            tail = tail->next;
            right = right->next;
        }
        else if ( strcmp(left->data, right->data) < 0 ) {
            tail->next = left;
            tail = tail->next;
            left = left->next;
        }
        else {  /* Nodes are equal */
            tail->next = left;
            tail = tail->next;
            tail->next = right;
            tail = tail->next;
            left = left->next;
            right = right->next;
        }
    }
    while (left) {
        tail->next = left;
        tail = tail->next;
        left = left->next;
    }
    while (right) {
        tail->next = right;
        tail = tail->next;
        right = right->next;
    }
    tail->next = NULL;
    tail = head->next;
    node_delete(head, false);
    return tail;
}
/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
