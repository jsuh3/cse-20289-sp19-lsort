/* lsort.c: Sorting Utility Using Linked-Lists */

#include "lsort.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "  -r   reverse the result of comparisons\n");
    exit(status);
}

/**
 * * lsort
 * *
 * * Determines whether or not a list is meant to be reversed, and if it is, reverses it.
 * * Then, outputs the list.
 * * @param   *stream   
 * * @param   reverse   Whether or not to reverse the list
 * * @return  NULL pointer.
 * */
void lsort(FILE *stream, bool reverse) {
    List *l = list_create();
    char buffer[BUFSIZ];
    while(fgets(buffer, BUFSIZ, stream)) {
        buffer[strlen(buffer) - 1] = 0;
        list_push_back(l, buffer);
    }
    list_sort(l);
    if(reverse) {
        list_reverse(l);
    }

    for (Node *curr = l->head; curr != NULL; curr = curr->next) {
        printf("%s\n", curr->data);
    }
    list_delete(l);
}

/* Main Execution */
/**
 * * main
 * *
 * * main function to test lsort
 * * @param   argc          Number of arguments
 * * @param   argv          arguments
 * * @return  NULL pointer.
 * */
int main(int argc, char *argv[]) {
    int  argind  = 1;
    bool reverse = false;

    /* Parse command line arguments */
    PROGRAM_NAME = argv[0];
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'r':
                reverse = true;
                break;
            case 'h':
                usage(0);
                break;
            default:
                usage(1);
                break;
        }
    }

    /* Sort using list */
    lsort(stdin, reverse);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
